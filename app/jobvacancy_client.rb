require 'json'

class JobVacancyClient
  def initialize
    @conn = Faraday.new(
      url: ENV['JOBVACANCY_BASE_URL'],
      headers: { 'Content-Type' => 'application/json' }
    )
  end

  def offers
    JSON.parse(@conn.get('/api/offers').body)
  end

  def today_offers(date)
    params = { date: Time.at(date) }.to_json
    JSON.parse(@conn.post('/api/offers/today', params).body)
  end

  def register_user(user_name, user_email, user_birth_date, user_password)
    params = { "name": user_name, "email": user_email, "birth_date": user_birth_date, "password": user_password }.to_json
    JSON.parse(@conn.post('/api/user', params).body)['message']
  end

  def apply(offer_id, applicant_email)
    params = { offer_id:, applicant_email: }.to_json
    @conn.post('/api/offers/apply', params)
  end
end
