require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require "#{File.dirname(__FILE__)}/jobvacancy_client"

class Routes
  include Routing

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}")
  end

  on_message '/stop' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Chau, #{message.from.username}")
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  if ENV['OFFERS_COMMAND'] == 'true'
    on_message '/offers' do |bot, message|
      job_offers = JobVacancyClient.new.offers

      response_message = if job_offers.empty?
                           'There are no offers'
                         else
                           prefix = "Job Offers:\n-----------\n"
                           prefix.concat(job_offers.map do |job_offer|
                             <<~TEXT.chomp
                               Offer ID: #{job_offer['id']}
                               Title: #{job_offer['title']}
                               Location: #{job_offer['location']}
                               Description: #{job_offer['description']}
                             TEXT
                           end.join("\n\n"))
                         end

      bot.api.send_message(chat_id: message.chat.id, text: response_message)
    end
  end

  if ENV['REGISTER_USER_COMMAND'] == 'true'
    on_message_pattern %r{/register_user (?<params>.*)} do |bot, message, args|
      name, email, birth_date, password = args['params'].split(' ')
      if name.nil? || email.nil? || birth_date.nil? || password.nil?
        bot.api.send_message(chat_id: message.chat.id, text: 'Invalid command usage. Try /register_user <name> <email> <birth_date> <password>')
      else
        response_message = JobVacancyClient.new.register_user(name, email, birth_date, password)
        bot.api.send_message(chat_id: message.chat.id, text: response_message)
      end
    end
  end

  if ENV['APPLY_COMMAND'] == 'true'
    on_message_pattern %r{/apply (?<params>.*)} do |bot, message, args|
      offer_id, applicant_email = args['params'].split(' ')
      if offer_id.empty? || applicant_email.nil?
        bot.api.send_message(chat_id: message.chat.id, text: 'Invalid command usage. Try /apply <offer_id> <email>')

      else
        JobVacancyClient.new.apply(offer_id.to_i, applicant_email)
        bot.api.send_message(chat_id: message.chat.id, text: 'Applied successfully')

      end
    end
  end

  if ENV['HELP_COMMAND'] == 'true'
    on_message_pattern %r{/help(?<params>.*)} do |bot, message|
      text = <<~TEXT.chomp
        /start - Start the bot
        /stop - Stop the bot
        /version - Show the bot version
        /help - Show this help
        #{'/offers - List job offers' if ENV['OFFERS_COMMAND'] == 'true'}
        #{'/apply <offer_id> <email> - Apply to a job offer' if ENV['APPLY_COMMAND'] == 'true'}
        #{'/register_user <name> <email> <birth_date> <password> - Register a new user' if ENV['REGISTER_USER_COMMAND'] == 'true'}
        #{'/today_offers - List job offers listed today' if ENV['TODAY_OFFERS_COMMAND'] == 'true'}
      TEXT
      bot.api.send_message(chat_id: message.chat.id, text:)
    end
  end

  if ENV['TODAY_OFFERS_COMMAND'] == 'true'
    on_message '/today_offers' do |bot, message|
      job_offers = JobVacancyClient.new.today_offers(message.date)

      response_message = if job_offers.empty?
                           'There are no offers created today'
                         else
                           prefix = "Job Offers:\n-----------\n"
                           prefix.concat(job_offers.map do |job_offer|
                             <<~TEXT.chomp
                               Offer ID: #{job_offer['id']}
                               Title: #{job_offer['title']}
                               Location: #{job_offer['location']}
                               Description: #{job_offer['description']}
                             TEXT
                           end.join("\n\n"))
                         end

      bot.api.send_message(chat_id: message.chat.id, text: response_message)
    end
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end
