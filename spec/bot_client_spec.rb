require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

TOKEN = 'fake_token'.freeze

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{TOKEN}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def then_i_get_text(message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{TOKEN}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_keyboard_message(message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{TOKEN}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def run_bot
  app = BotClient.new(TOKEN)
  app.run_once
end

def mock_jobvacancy_offers(response)
  stub_request(:any, 'https://jobvacancy.com/api/offers')
    .to_return(body: response.to_json, status: 200)
end

def mock_jobvacancy_today_offers(response)
  stub_request(:any, 'https://jobvacancy.com/api/offers/today')
    .to_return(body: response.to_json, status: 200)
end

def mock_jobvacancy_user(response, birth_date)
  stub_request(:any, 'https://jobvacancy.com/api/user')
    .with(
      body: { 'name' => 'Julian',
              'email' => 'user@test.com',
              'birth_date' => birth_date,
              'password' => '1234' }
    )
    .to_return(body: response.to_json, status: 200, headers: {})
end

describe 'BotClient' do
  it 'should get a /version message and respond with current version' do
    when_i_send_text('/version')
    then_i_get_text(Version.current)

    run_bot
  end

  it 'should get a /start message and respond with Hola' do
    when_i_send_text('/start')
    then_i_get_text('Hola, Emilio')

    run_bot
  end

  it 'should get a /stop message and respond with Chau' do
    when_i_send_text('/stop')
    then_i_get_text('Chau, egutter')

    run_bot
  end

  it 'should get an unknown message message and respond with Do not understand' do
    when_i_send_text('/unknown')
    then_i_get_text('Uh? No te entiendo! Me repetis la pregunta?')

    run_bot
  end

  describe 'should get a /offers message' do
    it 'and respond with "There are no offers" when there are no offers' do
      mock_jobvacancy_offers([])
      when_i_send_text('/offers')
      then_i_get_text('There are no offers')

      run_bot
    end

    # rubocop:disable RSpec/ExampleLength
    it 'and respond with offers info when there are offers' do
      expect_text = <<~TEXT.chomp
        Job Offers:
        -----------
        Offer ID: 123
        Title: Java SR
        Location: Remote
        Description: A description

        Offer ID: 456
        Title: Ruby SR
        Location: Argentina
        Description: Another description
      TEXT
      mock_jobvacancy_offers([
                               { id: 123, title: 'Java SR', location: 'Remote', description: 'A description' },
                               { id: 456, title: 'Ruby SR', location: 'Argentina', description: 'Another description' }
                             ])
      when_i_send_text('/offers')
      then_i_get_text(expect_text)

      run_bot
    end
  end

  describe 'should get a /register_user Julian user@test.com 2000-09-06 1234 message' do
    it 'and respond with "User created. Welcome Julian!" when there are no offers' do
      birth_date = Date.new(2000, 9, 6)
      mock_jobvacancy_user({ message: 'User created. Welcome Julian!' }, birth_date.to_s)
      when_i_send_text("/register_user Julian user@test.com #{birth_date} 1234")
      then_i_get_text('User created. Welcome Julian!')

      run_bot
    end

    it 'and respond with "User age must be greater than 18" when the user is below 18' do
      today_date = Date.new(2023, 6, 5)
      expected_message = 'User age must be greater than 18'

      allow(Date).to receive(:today).and_return today_date
      mock_jobvacancy_user({ message: expected_message }, today_date.to_s)

      when_i_send_text("/register_user Julian user@test.com #{today_date} 1234")
      then_i_get_text(expected_message)

      run_bot
    end

    it 'and respond with "Email already registered" when the user wants to register with a registered email' do
      today_date = Date.new(2000, 6, 5)
      expected_message = 'Email already registered'

      allow(Date).to receive(:today).and_return today_date
      mock_jobvacancy_user({ message: expected_message }, today_date.to_s)

      when_i_send_text("/register_user Julian user@test.com #{today_date} 1234")
      then_i_get_text(expected_message)

      run_bot
    end

    it 'without name and respond with Invalid command usage. Try /register_user <name> <email> <birth_date> <password>' do
      expected_message = 'Invalid command usage. Try /register_user <name> <email> <birth_date> <password>'
      when_i_send_text('/register_user user@test.com 2000-06-30 1234')
      then_i_get_text(expected_message)

      run_bot
    end

    it 'without email and respond with Invalid command usage. Try /register_user <name> <email> <birth_date> <password>' do
      expected_message = 'Invalid command usage. Try /register_user <name> <email> <birth_date> <password>'
      when_i_send_text('/register_user Julian 2000-06-30 1234')
      then_i_get_text(expected_message)

      run_bot
    end

    it 'without birth date and respond with Invalid command usage. Try /register_user <name> <email> <birth_date> <password>' do
      expected_message = 'Invalid command usage. Try /register_user <name> <email> <birth_date> <password>'
      when_i_send_text('/register_user Julian user@test.com 1234')
      then_i_get_text(expected_message)

      run_bot
    end

    it 'without passsword and respond with Invalid command usage. Try /register_user <name> <email> <birth_date> <password>' do
      expected_message = 'Invalid command usage. Try /register_user <name> <email> <birth_date> <password>'
      when_i_send_text('/register_user Julian user@test.com 2000-06-30')
      then_i_get_text(expected_message)

      run_bot
    end

    it 'without name and passsword and respond with Invalid command usage. Try /register_user <name> <email> <birth_date> <password>' do
      expected_message = 'Invalid command usage. Try /register_user <name> <email> <birth_date> <password>'
      when_i_send_text('/register_user user@test.com 2000-06-30')
      then_i_get_text(expected_message)

      run_bot
    end
    # rubocop:enable RSpec/ExampleLength
  end

  describe 'when an /apply command is received' do
    def mock_jobvacancy_offers_apply(status_code, response)
      stub_request(:any, 'https://jobvacancy.com/api/offers/apply')
        .with(
          body: { offer_id: 123, applicant_email: 'applicant@mail.com' }
        )
        .to_return(body: response.to_json, status: status_code)
    end

    it 'with email and offer id it should receive a message saying that applied successfully' do
      mock_jobvacancy_offers_apply(204, [])
      when_i_send_text('/apply 123 applicant@mail.com')
      then_i_get_text('Applied successfully')

      run_bot
    end

    it 'without offer id it should receive a message saying offer id is missing' do
      when_i_send_text('/apply applicant@mail.com')
      then_i_get_text('Invalid command usage. Try /apply <offer_id> <email>')

      run_bot
    end

    it 'without applicant email it should receive a message saying offer id is missing' do
      when_i_send_text('/apply 1234')
      then_i_get_text('Invalid command usage. Try /apply <offer_id> <email>')

      run_bot
    end
  end

  describe 'when an /help command is received' do
    let(:expected_help_message) do
      <<~TEXT.chomp
        /start - Start the bot
        /stop - Stop the bot
        /version - Show the bot version
        /help - Show this help
        /offers - List job offers
        /apply <offer_id> <email> - Apply to a job offer
        /register_user <name> <email> <birth_date> <password> - Register a new user
        /today_offers - List job offers listed today
      TEXT
    end

    it 'should respond with help info if there are no parameters' do
      when_i_send_text('/help')
      then_i_get_text(expected_help_message)

      run_bot
    end

    it 'should respond with help info' do
      when_i_send_text('/help <a_text>')
      then_i_get_text(expected_help_message)

      run_bot
    end
  end

  # rubocop:disable RSpec/ExampleLength
  describe 'when a /today_offers command is received' do
    let(:expected_today_offers_message) do
      <<~TEXT.chomp
        Job Offers:
        -----------
        Offer ID: 123
        Title: Java SR
        Location: Remote
        Description: A description

        Offer ID: 456
        Title: Ruby SR
        Location: Argentina
        Description: Another description
      TEXT
    end

    it 'should respond with the offers created today' do
      mock_jobvacancy_today_offers([
                                     { id: 123, title: 'Java SR', location: 'Remote', description: 'A description' },
                                     { id: 456, title: 'Ruby SR', location: 'Argentina', description: 'Another description' }
                                   ])
      when_i_send_text('/today_offers')
      then_i_get_text(expected_today_offers_message)

      run_bot
    end

    it 'should respond with there are no offers created today' do
      mock_jobvacancy_today_offers([])
      when_i_send_text('/today_offers')
      then_i_get_text('There are no offers created today')

      run_bot
    end
  end
  # rubocop:enable RSpec/ExampleLength
end
